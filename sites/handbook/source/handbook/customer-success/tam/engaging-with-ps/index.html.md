---
layout: handbook-page-toc
title: "Engaging with Professional Services"
description: "Guidelines for TAMs on how best to enagage with professional services."
---

## On this page

{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

### TAM Guidelines - Engaging with PS on a customer project

The following outlines the steps/means for a TAM to engage with Professional Services throughout a PS engagement with an existing customer.  Once a project is closed-won in Salesforce, PS Ops engages with the customer to [establish the key project criteria](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/project-mgmt/#initiate).  Once this criteria is received, the project manager is assigned.  The following steps are completed by the project manager:

1. Invites TAM to internal PS kick-off meeting (PSE, PM, TAM)
1. Invites TAM to Account Team (Sales) to PS hand-off meeting
1. Invites TAM to customer kick-off meeting
1. Invites TAM to join standups (weekly, daily, etc)
1. Invites TAM to PS slack channel
1. Invites TAM (as optional) to weekly status meeting with the customer and the entire project team.
1. Keeps TAM in the loop (and visa versa) regarding potential deal growth in either the licensing or services categories
1. Invites TAM to [project closure meeting ](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/project-mgmt/#deploy--close)

#### TAM Responsibility: 

1. TAM feeds key information back to the project team such as future licensing deals, upgrades, etc.
1. TAM keeps up to date with progress via (slack channel, daily standups, weekly status meetings, status reports sent via email, etc.)

